/*******************************************************************************
 * © Copyright 2015, James P White,  All Rights Reserved.
 * Created by Jim White on 11/12/2015.
 ******************************************************************************/
package jpwhite.com.androidamqp.collector;

import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;

import jpwhite.com.androidamqp.domain.GeoData;

public class GeoCollector implements LocationListener {

    private static final int TWO_MINUTES = 1000 * 60 * 2;
    private static final float METER_TO_MILE = 2.23693629f;
    private static final float METER_TO_KM = 0.001f;
    private static final double METER_TO_FEET = 3.2808399;

    private double[] lastFourAlts = {-1.0, -1.0, -1.0, -1.0};
    private LocationUoM uom = LocationUoM.English;
    private Location lastLocation;
    private double lastAltitude;
    private double lastSpeed;

    public GeoData getGeoData(){
        return new GeoData(getLastLocation().getLatitude(), getLastLocation().getLongitude(), getLastAltitude(), getLastSpeed());
    }

    public LocationUoM getUom() {
        return uom;
    }

    public void setUom(LocationUoM uom) {
        this.uom = uom;
    }

    public Location getLastLocation() {
        return lastLocation;
    }

    public void setLastLocation(Location lastLocation) {
        this.lastLocation = lastLocation;
    }

    public double getLastAltitude() {
        return lastAltitude;
    }

    public void setLastAltitude(double lastAltitude) {
        this.lastAltitude = lastAltitude;
    }

    public double getLastSpeed() {
        return lastSpeed;
    }

    public void setLastSpeed(double lastSpeed) {
        this.lastSpeed = lastSpeed;
    }

    @Override
    public void onLocationChanged(Location newLocation) {
        if (isBetterLocation(newLocation, lastLocation)){
            lastLocation = newLocation;

            double altitude = -1;
            if (newLocation.hasAltitude()) {
                altitude = newLocation.getAltitude();
                if (uom.equals(LocationUoM.English)) {
                    altitude = altitude * METER_TO_FEET;
                }
                lastAltitude = smootheCurve(altitude);
            }
            float myspeed = -1f;
            if (newLocation.hasSpeed()) {
                myspeed = newLocation.getSpeed();
                if (uom.equals(LocationUoM.English)) {
                    lastSpeed = myspeed * METER_TO_MILE;
                } else {
                    lastSpeed = myspeed * METER_TO_KM;
                }
            }
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    @Override
    public void onProviderEnabled(String provider) {
    }

    @Override
    public void onProviderDisabled(String provider) {
    }

    /**
     * Determines whether one Location reading is better than the current Location fix
     *
     */
    private boolean isBetterLocation(Location location, Location currentBestLocation) {
        if (currentBestLocation == null) {
            // A new location is always better than no location
            return true;
        }

        // Check whether the new location fix is newer or older
        long timeDelta = location.getTime() - currentBestLocation.getTime();
        boolean isSignificantlyNewer = timeDelta > TWO_MINUTES;
        boolean isSignificantlyOlder = timeDelta < -TWO_MINUTES;
        boolean isNewer = timeDelta > 0;

        // If it's been more than two minutes since the current location, use the new location
        // because the user has likely moved
        if (isSignificantlyNewer) {
            return true;
            // If the new location is more than two minutes older, it must be worse
        } else if (isSignificantlyOlder) {
            return false;
        }

        // Check whether the new location fix is more or less accurate
        int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation.getAccuracy());
        boolean isLessAccurate = accuracyDelta > 0;
        boolean isMoreAccurate = accuracyDelta < 0;
        boolean isSignificantlyLessAccurate = accuracyDelta > 200;

        // Check if the old and new location are from the same provider
        boolean isFromSameProvider = isSameProvider(location.getProvider(),
                currentBestLocation.getProvider());

        // Determine location quality using a combination of timeliness and accuracy
        if (isMoreAccurate) {
            return true;
        } else if (isNewer && !isLessAccurate) {
            return true;
        } else if (isNewer && !isSignificantlyLessAccurate && isFromSameProvider) {
            return true;
        }
        return false;
    }

    /**
     * Checks whether two providers are the same
     */
    private boolean isSameProvider(String provider1, String provider2) {
        if (provider1 == null) {
            return provider2 == null;
        }
        return provider1.equals(provider2);
    }

    /**
     * returns altitude off a smoothe curve so that results don't look odd
     */
    private double smootheCurve(double newAlt){
        System.arraycopy(lastFourAlts, 0, lastFourAlts, 1, 3);
        lastFourAlts[0] = newAlt;
        double sum = 0.0f;
        int cnt = 0;
        for (int i = 0; i < lastFourAlts.length; i++) {
            if (lastFourAlts[i] > 0) {
                sum = sum + lastFourAlts[i];
                cnt++;
            }
        }
        return sum/cnt;
    }
}
