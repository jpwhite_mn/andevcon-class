/*******************************************************************************
 * © Copyright 2015, James P White, Inc.  All Rights Reserved.
 * Created by Jim White on 11/12/2015.
 ******************************************************************************/
package jpwhite.com.androidamqp.domain;

public class LightData {

    private float level;

    public LightData(float level) {
        this.level = level;
    }

    public float getLevel() {

        return level;
    }

    public void setLevel(float level) {
        this.level = level;
    }

    @Override
    public String toString() {
        return "level=" + level +
                "\n";
    }
}
