/*******************************************************************************
 * © Copyright 2015, James P White,  All Rights Reserved.
 * Created by Jim White on 11/12/2015.
 ******************************************************************************/
package jpwhite.com.androidamqp;

import android.util.Log;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

public class DataSender {
    private static final String URI = "amqp://vvklkgfo:nueVJUuq63PyDjnlAPRQLWk2ZbDoCd5k@moose.rmq.cloudamqp.com/vvklkgfo";
    private static final String Q_NAME = "andevcon";
    private static final String TAG = "DataSendor";

    public static void sendJson(String json) {
        try {
            ConnectionFactory factory = new ConnectionFactory();
            factory.setAutomaticRecoveryEnabled(false);
            factory.setUri(URI);
            Connection connection = factory.newConnection();
            Channel channel = connection.createChannel();
            channel.queueDeclare(Q_NAME, false, false, false, null);
            channel.basicPublish("", Q_NAME,null, json.getBytes());
            channel.close();
            connection.close();
        } catch (Exception e) {
            Log.e(TAG, "Problem sending data " + e.getLocalizedMessage());
        }
    }
}
