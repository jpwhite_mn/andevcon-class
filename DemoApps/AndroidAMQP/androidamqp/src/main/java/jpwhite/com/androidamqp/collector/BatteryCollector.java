/*******************************************************************************
 * © Copyright 2015, James P White, All Rights Reserved.
 * Created by Jim White on 11/12/2015.
 ******************************************************************************/
package jpwhite.com.androidamqp.collector;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.BatteryManager;

import jpwhite.com.androidamqp.domain.BatteryData;

public class BatteryCollector extends BroadcastReceiver {

    private int health;
    private int level;
    private int temperature;
    private int voltage;

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getTemperature() {
        return temperature;
    }

    public void setTemperature(int temperature) {
        this.temperature = temperature;
    }

    public int getVoltage() {
        return voltage;
    }

    public void setVoltage(int voltage) {
        this.voltage = voltage;
    }

    public BatteryData getBatteryData() {
        return new BatteryData(getHealth(), getLevel(), getTemperature(), getVoltage());
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        health = intent.getIntExtra(BatteryManager.EXTRA_HEALTH, 0);
        level = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, 0);
        temperature = intent.getIntExtra(BatteryManager.EXTRA_TEMPERATURE, 0);
        voltage = intent.getIntExtra(BatteryManager.EXTRA_VOLTAGE, 0);
    }
}
