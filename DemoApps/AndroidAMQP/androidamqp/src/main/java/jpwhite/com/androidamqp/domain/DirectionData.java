/*******************************************************************************
 * © Copyright 2015, James P White, Inc.  All Rights Reserved.
 * Created by Jim White on 11/12/2015.
 ******************************************************************************/
package jpwhite.com.androidamqp.domain;

public class DirectionData {

    private float direction;

    public float getDirection() {
        return direction;
    }

    public void setDirection(float direction) {
        this.direction = direction;
    }

    public DirectionData(float direction) {
        this.direction = direction;
    }

    @Override
    public String toString() {
        return "direction=" + direction +
                "\n";
    }
}
