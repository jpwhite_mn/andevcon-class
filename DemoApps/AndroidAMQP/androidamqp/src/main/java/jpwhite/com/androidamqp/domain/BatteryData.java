/*******************************************************************************
 * © Copyright 2015, James P White, Inc.  All Rights Reserved.
 * Created by Jim White on 11/12/2015.
 ******************************************************************************/
package jpwhite.com.androidamqp.domain;

public class BatteryData {
    private int health;
    private int level;
    private int temperature;
    private int voltage;

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getTemperature() {
        return temperature;
    }

    public void setTemperature(int temperature) {
        this.temperature = temperature;
    }

    public int getVoltage() {
        return voltage;
    }

    public void setVoltage(int voltage) {
        this.voltage = voltage;
    }

    public BatteryData(int health, int level, int temperature, int voltage) {

        this.health = health;
        this.level = level;
        this.temperature = temperature;
        this.voltage = voltage;
    }

    @Override
    public String toString() {
        return "health=" + health + "\nlevel=" + level +
                "\ntemperature=" + temperature +
                "\nvoltage=" + voltage +
                "\n";
    }
}
