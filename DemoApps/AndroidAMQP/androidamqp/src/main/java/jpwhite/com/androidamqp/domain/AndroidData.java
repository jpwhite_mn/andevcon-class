/*******************************************************************************
 * © Copyright 2015, James P White, Inc.  All Rights Reserved.
 * Created by Jim White on 11/12/2015.
 ******************************************************************************/
package jpwhite.com.androidamqp.domain;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class AndroidData {
    private static final Gson gson = new GsonBuilder().serializeSpecialFloatingPointValues().create();
    private BatteryData batteryData;
    private DirectionData directionData;
    private GeoData geoData;
    private LightData lightData;
    private String deviceName;

    public AndroidData(String name){
        this.deviceName = name;
    }

    public BatteryData getBatteryData() {
        return batteryData;
    }

    public void setBatteryData(BatteryData batteryData) {
        this.batteryData = batteryData;
    }

    public DirectionData getDirectionData() {
        return directionData;
    }

    public void setDirectionData(DirectionData directionData) {
        this.directionData = directionData;
    }

    public GeoData getGeoData() {
        return geoData;
    }

    public void setGeoData(GeoData geoData) {
        this.geoData = geoData;
    }

    public LightData getLightData() {
        return lightData;
    }

    public void setLightData(LightData lightData) {
        this.lightData = lightData;
    }

    public String getJson() {
        return gson.toJson(this);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (batteryData != null){
            builder.append(batteryData.toString());
        }
        if (directionData != null){
            builder.append(directionData.toString());
        }
        if (geoData != null){
            builder.append(geoData.toString());
        }
        if (lightData != null) {
            builder.append(lightData.toString());
        }
        return builder.toString();
    }
}
