/*******************************************************************************
 * © Copyright 2015, James P White,  All Rights Reserved.
 * Created by Jim White on 11/12/2015.
 ******************************************************************************/
package jpwhite.com.androidamqp.collector;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;

import jpwhite.com.androidamqp.domain.LightData;

public class LightCollector implements SensorEventListener {

    private float lastLightLevel;

    public LightData getLightData() {
        return new LightData(getLastLightLevel());
    }

    public float getLastLightLevel() {
        return lastLightLevel;
    }

    public void setLastLightLevel(float lastLightLevel) {
        this.lastLightLevel = lastLightLevel;
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        lastLightLevel = event.values[0];
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }
}
