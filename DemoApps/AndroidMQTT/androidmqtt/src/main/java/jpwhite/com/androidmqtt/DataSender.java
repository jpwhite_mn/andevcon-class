/*******************************************************************************
 * © Copyright 2015, James P White,  All Rights Reserved.
 * Created by Jim White on 11/12/2015.
 ******************************************************************************/
package jpwhite.com.androidmqtt;

import android.util.Log;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttMessage;

public class DataSender {
    private static final String TOPIC = "andevcon";
    private static final int QOS = 0;
    private static final String BROKER = "tcp://m11.cloudmqtt.com:18465";
    private static final String CLIENT_ID = "AndroidSubscriber";
    private static final String USERNAME = "dnmiblsm";
    private static final String PASSWORD = "kubhPPyF5l6U";

    private static final String TAG = "DataSendor";

    public static void sendJson(String json) {
        try {
            MqttClient client = new MqttClient(BROKER, CLIENT_ID, null);
            MqttConnectOptions connOpts = new MqttConnectOptions();
            connOpts.setUserName(USERNAME);
            connOpts.setPassword(PASSWORD.toCharArray());
            connOpts.setCleanSession(true);
            client.connect(connOpts);
            MqttMessage message = new MqttMessage(json.getBytes());
            message.setQos(QOS);
            message.setRetained(false);
            client.publish(TOPIC, message);
            client.disconnect();
        } catch (Exception e) {
            Log.e(TAG, "Problem sending data " + e.getLocalizedMessage());
        }
    }
}
