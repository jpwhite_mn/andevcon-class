/*******************************************************************************
 * © Copyright 2015, James P White,  All Rights Reserved.
 * Created by Jim White on 11/12/2015.
 ******************************************************************************/
package jpwhite.com.androidmqtt;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

public class MainActivityFragment extends Fragment {

    private Button startButton, stopButton;
    private TextView resultsTextView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main,
                container, false);
        startButton = (Button) rootView.findViewById(R.id.startButton);
        stopButton = (Button) rootView.findViewById(R.id.stopButton);
        resultsTextView = (TextView) rootView
                .findViewById(R.id.resultsTextView);
        View.OnClickListener listener = new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (v == startButton) {
                    ((MainActivity) getActivity())
                            .startGetSend();
                } else {
                    ((MainActivity) getActivity())
                            .stopGetSend();
                }
            }
        };
        startButton.setOnClickListener(listener);
        stopButton.setOnClickListener(listener);
        return rootView;
    }

    public TextView getResultsTextView() {
        return resultsTextView;
    }
}
