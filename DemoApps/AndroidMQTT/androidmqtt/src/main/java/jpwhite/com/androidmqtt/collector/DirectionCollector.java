/*******************************************************************************
 * © Copyright 2015, James P White,  All Rights Reserved.
 * Created by Jim White on 11/12/2015.
 ******************************************************************************/
package jpwhite.com.androidmqtt.collector;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.view.Surface;

import jpwhite.com.androidmqtt.domain.DirectionData;

public class DirectionCollector implements SensorEventListener {

    private float lastDirection;

    private int rotation;

    public DirectionCollector(int rotation) {
        this.rotation = rotation;
    }

    public float getLastDirection() {
        return lastDirection;
    }

    public void setLastDirection(float lastDirection) {
        this.lastDirection = lastDirection;
    }

    public DirectionData getDirectionData() {
        return new DirectionData(getLastDirection());
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        if (accuracy <= 1) {

        }
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        float direction = event.values[0];
        switch (rotation) {
            case Surface.ROTATION_90:
                direction = direction + 90;
                break;
            case Surface.ROTATION_180:
                direction = direction - 180;
                break;
            case Surface.ROTATION_270:
                direction = direction - 90;
                break;
            default:
                break;
        }
        if (direction < 0) {
            direction = 360 + direction;
        }
        if (direction > 360) {
            direction = direction - 360;
        }
        lastDirection = direction;
    }
}
