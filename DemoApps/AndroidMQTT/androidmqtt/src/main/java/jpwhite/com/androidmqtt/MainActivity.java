/*******************************************************************************
 * © Copyright 2015, James P White,  All Rights Reserved.
 * Created by Jim White on 11/12/2015.
 ******************************************************************************/
package jpwhite.com.androidmqtt;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;

import jpwhite.com.androidmqtt.collector.BatteryCollector;
import jpwhite.com.androidmqtt.collector.DirectionCollector;
import jpwhite.com.androidmqtt.collector.GeoCollector;
import jpwhite.com.androidmqtt.collector.LightCollector;
import jpwhite.com.androidmqtt.domain.AndroidData;

public class MainActivity extends Activity {

    private static final int LOCATION_INTERVAL = 1000; // in milliseconds
    private static final int MIN_DISTANCE = 10; // in meters
    private static final boolean USE_GPS = true; // when false, use WI_FI
    private static final String DEVICE_NAME = "JimsPhone";

    MainActivityFragment mainFrag;
    GetSendDeviceStats work;
    boolean isBlack = true;
    boolean isRunning = false;
    GeoCollector geoCollector;
    DirectionCollector directionCollector;
    LightCollector lightCollector;
    BatteryCollector batteryCollector;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState == null) {
            mainFrag = new MainActivityFragment();
            getFragmentManager().beginTransaction()
                    .add(R.id.container, mainFrag).commit();
        } else {
            mainFrag = (MainActivityFragment) getFragmentManager().findFragmentById(R.id.container);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        startDirectionService();
        startLocationServices();
        startLightService();
        startBatteryService();
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopDirectionService();
        stopLocationServices();
        stopLightService();
        stopBatteryService();
        stopGetSend();
    }

    public void startGetSend() {
        if (!isRunning) {
            work = new GetSendDeviceStats(5000);
            work.start();
            isRunning = true;
        }
    }

    public void stopGetSend() {
        if (isRunning) {
            work.interrupt();
            updateResults(getString(R.string.service_off));
            isBlack = false;
            isRunning = false;
        }
    }

    public void updateResults(String results) {
        if (mainFrag != null) {
            mainFrag.getResultsTextView().setText(results);
            if (isBlack) {
                mainFrag.getResultsTextView().setTextColor(getResources().getColor(R.color.red));
                isBlack = false;
            } else {
                mainFrag.getResultsTextView().setTextColor(getResources().getColor(R.color.black));
                isBlack = true;
            }
        }
    }

    private void startBatteryService() {
        batteryCollector = new BatteryCollector();
        this.registerReceiver(batteryCollector, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
    }

    private void stopBatteryService() {
        if (batteryCollector != null) {
            this.unregisterReceiver(batteryCollector);
        }
    }

    private void startLightService() {
        SensorManager sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        lightCollector = new LightCollector();
        Sensor sensor = sensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);
        sensorManager.registerListener(lightCollector, sensor, SensorManager.SENSOR_DELAY_NORMAL);
    }

    private void stopLightService() {
        if (lightCollector != null) {
            SensorManager sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
            sensorManager.unregisterListener(lightCollector);
        }
    }

    private void startDirectionService() {
        SensorManager sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        int rotation = this.getWindowManager().getDefaultDisplay()
                .getRotation();
        directionCollector = new DirectionCollector(rotation);
        Sensor sensor = sensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION);
        sensorManager.registerListener(directionCollector, sensor,
                SensorManager.SENSOR_DELAY_UI);
    }

    private void stopDirectionService() {
        if (directionCollector != null) {
            SensorManager sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
            sensorManager.unregisterListener(directionCollector);
        }
    }

    private void startLocationServices() {
        LocationManager locMgr = (LocationManager) getSystemService(LOCATION_SERVICE);
        geoCollector = new GeoCollector();

        if (USE_GPS) {
            geoCollector.setLastLocation(locMgr.getLastKnownLocation(LocationManager.GPS_PROVIDER));
            locMgr.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                    LOCATION_INTERVAL, MIN_DISTANCE, geoCollector);
        } else {
            geoCollector.setLastLocation(locMgr.getLastKnownLocation(LocationManager.NETWORK_PROVIDER));
            locMgr.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,
                    LOCATION_INTERVAL, MIN_DISTANCE, geoCollector);
        }
    }

    private void stopLocationServices() {
        if (geoCollector != null) {
            LocationManager locMgr = (LocationManager) getSystemService(LOCATION_SERVICE);
            locMgr.removeUpdates(geoCollector);
        }
    }

    public class GetSendDeviceStats extends Thread {

        private static final String TAG = "GetSendDeviceStats";
        private int delay = 5000;  // 5 seconds
        private static final int RANDOM_MULTIPLIER = 10;

        public GetSendDeviceStats(int delay) {
            this.delay = delay;
        }

        @Override
        public void run() {
            AndroidData data = new AndroidData(DEVICE_NAME);
            publishProgress("updating ...");
            while (true) {
                if (geoCollector != null) {
                    if (geoCollector.getLastLocation() != null) {
                        data.setGeoData(geoCollector.getGeoData());
                    }
                }
                if (directionCollector != null) {
                    data.setDirectionData(directionCollector.getDirectionData());
                }
                if (lightCollector != null) {
                    data.setLightData(lightCollector.getLightData());
                }
                if (batteryCollector != null) {
                    data.setBatteryData(batteryCollector.getBatteryData());
                }
                pushToCloud(data.getJson());
                publishProgress(data.toString());
                try {
                    Thread.sleep(delay);
                } catch (InterruptedException e) {
                    Log.i(TAG,
                            "Interrupting and stopping the Get/Send Device Stats Thread");
                    return;
                }
            }
        }

        private void publishProgress(final String data) {
            runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    updateResults(data);
                }
            });
        }

        private void pushToCloud(String jsonData){
            DataSender.sendJson(jsonData);
        }

    }
}
