/*******************************************************************************
 * Copyright 2015, James P White,  All Rights Reserved.
 * Created by Jim White on 11/12/2015.
 ******************************************************************************/
package com.jpwhite.sensorinfo;

import android.app.Application;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorManager;

import java.util.ArrayList;
import java.util.List;

public class SensorApplication extends Application{

    private List<Sensor> sensors;
    private List<String> sensorNames;

    public List<Sensor> getSensors(){
        if (sensors == null) {
            SensorManager sensorManager = (SensorManager) this.getSystemService(Context.SENSOR_SERVICE);
            sensors =  sensorManager.getSensorList(Sensor.TYPE_ALL);
            sensorNames = new ArrayList<>();
            for (Sensor sensor: sensors) {
                sensorNames.add((sensor.getName()));
            }
        }
        return sensors;
    }

    public List<String> getSensorNames() {
        if (sensorNames == null)
            getSensors();
        return sensorNames;
    }

    public Sensor getSensor(String position){
        if (sensors == null){
            getSensors();
        }
        int pos = Integer.parseInt(position);
        return sensors.get(pos);
    }

}
