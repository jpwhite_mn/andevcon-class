/*******************************************************************************
 * Copyright 2015, James P White,  All Rights Reserved.
 * Created by Jim White on 11/12/2015.
 ******************************************************************************/
package com.jpwhite.sensorinfo;

import android.hardware.Sensor;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.HashMap;

public class SensorDetailFragment extends Fragment {
    public static final String ARG_ITEM_ID = "item_id";

    private static HashMap<Integer, String> sensorTypes = new HashMap<>();
    private static HashMap<Integer, String> reportingTypes = new HashMap<>();


    static {
        sensorTypes.put(Sensor.TYPE_ACCELEROMETER, "Accelerometer");
        sensorTypes.put(Sensor.TYPE_AMBIENT_TEMPERATURE, "Ambient temperature");
        sensorTypes.put(Sensor.TYPE_GAME_ROTATION_VECTOR, "Game rotation vector");
        sensorTypes.put(Sensor.TYPE_GEOMAGNETIC_ROTATION_VECTOR, "Geomagnetic rotation vector");
        sensorTypes.put(Sensor.TYPE_GRAVITY, "Gravity");
        sensorTypes.put(Sensor.TYPE_GYROSCOPE, "Gyroscope");
        sensorTypes.put(Sensor.TYPE_GYROSCOPE_UNCALIBRATED, "Gyroscope (uncalibrated)");
        sensorTypes.put(Sensor.TYPE_HEART_RATE, "Heart rate");
        sensorTypes.put(Sensor.TYPE_LIGHT, "Light");
        sensorTypes.put(Sensor.TYPE_LINEAR_ACCELERATION, "Linear acceleration");
        sensorTypes.put(Sensor.TYPE_MAGNETIC_FIELD, "Magnetic field");
        sensorTypes.put(Sensor.TYPE_MAGNETIC_FIELD_UNCALIBRATED, "Magnetic field (uncalibrated)");
        sensorTypes.put(Sensor.TYPE_PRESSURE, "Pressure");
        sensorTypes.put(Sensor.TYPE_PROXIMITY, "Proximity");
        sensorTypes.put(Sensor.TYPE_RELATIVE_HUMIDITY, "Relative Humidity");
        sensorTypes.put(Sensor.TYPE_ROTATION_VECTOR, "Rotation vector");
        sensorTypes.put(Sensor.TYPE_SIGNIFICANT_MOTION, "Significant motion");
        sensorTypes.put(Sensor.TYPE_STEP_COUNTER, "Step counter");
        sensorTypes.put(Sensor.TYPE_STEP_DETECTOR, "Step detecor");
        reportingTypes.put(Sensor.REPORTING_MODE_CONTINUOUS, "Continuous");
        reportingTypes.put(Sensor.REPORTING_MODE_ON_CHANGE, "On change");
        reportingTypes.put(Sensor.REPORTING_MODE_ONE_SHOT, "One shot");
        reportingTypes.put(Sensor.REPORTING_MODE_SPECIAL_TRIGGER, "Special");
        sensorTypes.put(Sensor.TYPE_ORIENTATION, "Orientation");
        sensorTypes.put(Sensor.TYPE_TEMPERATURE, "Temperature");
    }

    private Sensor mItem;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments().containsKey(ARG_ITEM_ID)) {
            String sensorId = (String) getArguments().get(ARG_ITEM_ID);
            mItem = ((SensorApplication) getActivity().getApplication()).getSensor(sensorId);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_sensor_detail, container, false);

        if (mItem != null) {
            ((TextView) rootView.findViewById(R.id.nameTV)).setText(mItem.getName());
            ((TextView) rootView.findViewById(R.id.typeTV)).setText("type:  " + sensorTypes.get(mItem.getType()) + " (type #" + mItem.getType() + ")");
            ((TextView) rootView.findViewById(R.id.vendorTV)).setText("vendor:  " + mItem.getVendor());
            ((TextView) rootView.findViewById(R.id.versionTV)).setText("version:  " + mItem.getVersion());
            ((TextView) rootView.findViewById(R.id.powerTV)).setText("power usage:  " + mItem.getPower() + "mA");
            ((TextView) rootView.findViewById(R.id.resolutionTV)).setText("resolution:  " + mItem.getResolution());
            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                ((TextView) rootView.findViewById(R.id.maxRangeTV)).setText("max range:  " + mItem.getMaximumRange());
                ((TextView) rootView.findViewById(R.id.maxDelayTV)).setText("max delay:  " + mItem.getMaxDelay());
                ((TextView) rootView.findViewById(R.id.minDelayTV)).setText("min delay:  " + mItem.getMinDelay());
                ((TextView) rootView.findViewById(R.id.reportingModeTV)).setText("reporting mode:  " + reportingTypes.get(mItem.getReportingMode()));
            }
        }
        return rootView;
    }
}
