# README #

This repository contains the materials for the **Android IoT** class presented by **Jim White** @ AnDevCon 2015, Santa Clara.

### Repository Contents ###

* Class Slides (AndroidIoT.pptx)
* Demo applications (complete Android Studio projects provided)

© Copyright 2015, James P White,  All Rights Reserved.